(ns ftp
  (:refer-clojure :exclude [get])
  (:require [remote-shell.classpath :refer [add-dependencies]]
            [clojure.core :exclude [get]]
            [clojure.java.io :as io]))

(add-dependencies
 ["http://central.maven.org/maven2/commons-net/commons-net/3.5/commons-net-3.5.jar"])

(import 'org.apache.commons.net.ftp.FTPClient)

(defn new-client []
  (FTPClient.))

(defn connect [client host user password]
  (doto client
    (.connect host)
    (.login user password)
    .enterLocalPassiveMode))

(defn quit [client]
  (doto client
    .logout
    .disconnect))

(defn put [client local remote]
  (with-open [in-str (io/input-stream local)]
    (.storeFile client remote in-str)))

(defn get [client remote local]
  (with-open [out-str (io/output-stream local)]
    (.retrieveFile client remote out-str)))

(defn pwd [client]
  (.printWorkingDirectory client))

(defn ls
  ([client path]
   (print (.getStatus client path)))
  ([client]
   (ls client (pwd client))))

(defn cd [client path]
  (.changeWorkingDirectory client path))

(defn status [client]
  (print (.getStatus client)))

(defn connected? [client]
  (.isConnected client))
