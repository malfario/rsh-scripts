(ns ruby
  (:refer-clojure :exclude [eval])
  (:require [remote-shell.classpath :refer [add-dependency]])
  (:import [javax.script
            ScriptEngine
            ScriptEngineManager
            ScriptException]))

(add-dependency "https://s3.amazonaws.com/jruby.org/downloads/9.1.5.0/jruby-complete-9.1.5.0.jar")

(defn new-engine []
  (-> (ScriptEngineManager.) (.getEngineByName "jruby")))

(defonce engine (new-engine))

(defn eval [script]
  (.eval engine script))
